import {Component, ElementRef, OnInit, ViewEncapsulation} from '@angular/core';
import { FormControl } from '@angular/forms';
import {Chat} from "../../models/chat";
import {UserServiceService} from "../../Service/user-service.service";
import {ChatService} from "../../Service/chat.service";
import {forkJoin} from "rxjs";
import {LoginService} from "../../Service/login.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HomeComponent   implements OnInit{
  allUsers: any ;
  allMessages!: any [] ;
  messageControl = new FormControl('');
  chatListControl = new FormControl('');
  currentWritingUser!: string;
  emphanger: any;
  userselect = false;
  constructor(private elementRef: ElementRef,
              public userService: UserServiceService,
              private chatService: ChatService,
              private loginservice: LoginService ) {}
  ngOnInit(): void {
    this.setCurrentUser();
    forkJoin([
      this.chatService.getAllmessage(),
      this.userService.getAllUsers()
    ]).subscribe(([allMessages, allUsers]) => {
      this.allMessages = allMessages;
      console.log(this.allMessages);
      this.allUsers = allUsers;
      console.log(this.allUsers);
    });

  }

  color = '#111b21';
  searchTerm = '';

  curentUser: any;
  chatmessages: any = null;
  protected  chat : Chat | undefined ;

  /**
   * emöglicht ein Hell oder dunkles theme
   * @param name
   * @param value
   */
  setCssVariable(name: string, value: string): void {
    this.elementRef.nativeElement.style.setProperty(name, value);
  }

  /**
   * sendet eine nachricht
   * bevor kann man einen user auswählen, sonst wird der aktuelle user als empfänger gesetzt
   */
  sendMessage() {
    if(this.userselect === false){
    this.selectUser(this.curentUser)
      }
    const message = this.messageControl.value;
    //console.log(message);
    if (typeof message === 'string') {
      if(message !== '' && message !== ' '){
        const message = this.messageControl.value;
        this.chatService.sendMessage(message, this.emphanger.id, this.curentUser.id).subscribe({
          next: (data) => {
            //console.log(data.message);
            this.allMessages.push(data);
          },
          error: (error) => {
            console.log(error);
          }
        });
      }
    }else {
      console.log('Error: message is not a string')
    }
    this.messageControl.setValue('');

  }

  /**
   * man kann user auswählen, bevor man eine nachricht schreibt
   * @param user
   */
  async selectUser(user: any) {
    this.userselect = true;
    this.currentWritingUser = `${user.firstname} ${user.lastname}`;
    this.emphanger = user;
    console.log('TEST --> ' + JSON.stringify(user));
    this.allMessages= await this.chatService.getChatMessages(this.curentUser.id, user.id);
  }

  /**
   * setzt den aktuellen user
   *
   */
  setCurrentUser() {
    this.curentUser = this.loginservice.getLocalStorage();
    console.log(this.curentUser);
  }

  /**
   * gibt den Namen des senders zurück
   * @param senderID
   */
  getMessagesenderName(senderID: string) {
    for(let i = 0; i < this.allUsers.length; i++) {
      if(this.allUsers[i].id === senderID) {
        return this.allUsers[i].firstname;
      }
    }
  }

}
