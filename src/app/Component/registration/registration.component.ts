import { Component } from '@angular/core';
import {LoginService} from "../../Service/login.service";
import {Router} from "@angular/router";


@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent {
  constructor(public router: Router,
              private loginService: LoginService) { }
  registration(firstname: string, lastname: string, email: string, password: string, passwordConfirm: string) {
    console.log("registration");
    this.loginService.register(firstname, lastname, email, password, passwordConfirm).then(r => { console.log(r); } );
    this.router.navigate(['home']);
  }


}
