import { Component } from '@angular/core';
import { Router } from '@angular/router';
import {LoginService} from "../../Service/login.service";
@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent {
  constructor(public router: Router,
              private LoginService: LoginService) {

  }

  login(email: string, password: string) {
    console.log('login');
    this.LoginService.login(email, password);
    this.router.navigate(['home']);
  }
}
