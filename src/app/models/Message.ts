export class Message {
  constructor(message: string | null, emphangerID: string, senderID: string) {
    this.text = message;
    this.senderId = senderID;
    this.empfaengerId = emphangerID;
    this.sentDate = new Date();
  }
  text: string | null;
  senderId: string | undefined;
  empfaengerId?: string;
  sentDate: Date | undefined;
}
