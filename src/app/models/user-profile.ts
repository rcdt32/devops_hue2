export class ProfileUser {
  uid: number | undefined;
  firstname: string | undefined;
  lastname: string | undefined;
  email?: string;
  avatar?: string;
}
