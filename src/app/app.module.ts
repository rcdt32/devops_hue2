import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { SignInComponent } from './Component/sign-in/sign-in.component';
import { HomeComponent} from './Component/home/home.component';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatMenuModule} from '@angular/material/menu';
import {MatIconModule} from "@angular/material/icon";
import {MatSidenavModule} from "@angular/material/sidenav";
import {MatButtonModule} from "@angular/material/button";
import {MatDividerModule} from "@angular/material/divider";
import {MatAutocompleteModule} from "@angular/material/autocomplete";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatInputModule} from "@angular/material/input";
import {MatListModule} from "@angular/material/list";
import {MatToolbarModule} from "@angular/material/toolbar";
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { SearchFilterPipe } from './shared/pipe/search-filter.pipe';
import { DateDisplayPipe } from './shared/pipe/date-display.pipe';
import {HttpClientModule} from "@angular/common/http";
import { RegistrationComponent } from './Component/registration/registration.component';

@NgModule({
  declarations: [
    AppComponent,
    SignInComponent,
    HomeComponent,
    SearchFilterPipe,
    DateDisplayPipe,
    RegistrationComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([
      {path: '', component: SignInComponent},
      {path: 'home', component: HomeComponent},
      {path: 'registration', component: RegistrationComponent}
    ]),
    MatSlideToggleModule,
    BrowserAnimationsModule,
    MatMenuModule,
    MatIconModule,
    MatSidenavModule,
    MatButtonModule,
    MatDividerModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    MatInputModule,
    MatListModule,
    FormsModule,
    MatToolbarModule,
    Ng2SearchPipeModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
