import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchFilter',
})
export class SearchFilterPipe implements PipeTransform {
  transform(
    value: { firstname: string; lastname: string }[],
    search: string
  ): { firstname: string; lastname: string }[] {
    if (!value || !value.length) {
      return [];
    }
    const regexp = new RegExp(search, 'i');
    const properties = Object.keys(value[0]);
    return [
      ...value.filter((item) => {
        return properties.some((property) => regexp.test(item[property as keyof typeof item]));
      }),
    ];
  }
}
