import { Pipe, PipeTransform } from '@angular/core';
import {DatePipe} from "@angular/common";

@Pipe({
  name: 'dateDisplay'
})
export class DateDisplayPipe implements PipeTransform {

  transform(date: Date | undefined): string {
    if(( new DatePipe('en-US').transform(date, 'dd.MM.yyyy') == new DatePipe('en-US').transform(new Date(), 'dd.MM.yyyy'))){
      return new DatePipe('en-US').transform(date, 'HH:mm') ?? '';
    }else return new DatePipe('en-US').transform(date, 'dd.MM.yyyy HH:mm') ?? '';
  }

}
