import {Component} from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  title = 'Ndemia';
  constructor(public router: Router) {
  }
  width = "0";
  marginleft = "0";
  openNav() {
    this.width = "25%";
    this.marginleft = "25%";
  }

  /* Set the width of the sidebar to 0 and the left margin of the page content to 0 */
  closeNav() {
    this.width = "0px";
    this.marginleft = "0px";
  }

  navige(command: string) {
    this.closeNav();
    this.router.navigate([command]);
  }
}
