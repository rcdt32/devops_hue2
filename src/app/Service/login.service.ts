import { Injectable } from '@angular/core';
import {UserServiceService} from "./user-service.service";

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  data: any;
  constructor(private userService:UserServiceService) {
  }
  async login(email: string, password: string) {
    const data = await this.userService.pb.collection('users').authWithPassword(email, password);
    this.data = data.record;
    this.userService.currentUser = data.record;
    console.log(this.data);
    console.log(this.data.firstname);
    const myData = {
      "id": this.data.id,
      "firstname": this.data.firstname,
      "lastname": this.data.lastname,
      "email": this.data.email,
    }
    localStorage.setItem('currentUser', JSON.stringify(myData));
    const getDataFromLocalStorrage: any = localStorage.getItem('currentUser') as any;
    console.log(JSON.parse(getDataFromLocalStorrage).firstname);
  }
  setLocalStorage(data: any){
    localStorage.setItem('currentUser', JSON.stringify(data));
  }
  getLocalStorage(){
    const getDataFromLocalStorrage: any = localStorage.getItem('currentUser') as any;
    return JSON.parse(getDataFromLocalStorrage);
  }
  async  register(firstname: string, lastname: string, email: string, password: string, passwordConfirm: string) {
    if(password === passwordConfirm && password.length > 6
    ){
      const data = {
        "email": email,
        "emailVisibility": true,
        "password": password,
        "passwordConfirm": passwordConfirm,
        "firstname": firstname,
        "lastname": lastname,
      };
      this.userService.pb.collection('users').create(data);
      this.data = data;
      this.userService.currentUser = data;
      console.log(this.data);
      console.log(this.data.record);
    }
  }
}
