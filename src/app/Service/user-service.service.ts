import { Injectable } from '@angular/core';
import PocketBase from 'pocketbase';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {
  public currentUser: any;
  url = 'http://10.48.19.66:8090'
  pb = new PocketBase(this.url);
  constructor(private http: HttpClient) { }
  async getmyUsers() {
    const resultList = await this.pb.collection('users').getList(1, 50, {
      filter: 'created >= "2022-01-01 00:00:00" && id = "dkd2qw8k64u1ly2"',
    });
    console.log(resultList.items);
    console.log("avec item 0");
    console.log(resultList.items[0].id);
  }
  /**
   * Get all users  from the server
   * @returns {Observable<alluser>}
   */
  getAllUsers(): Observable<any>   {
    return this.http.get<any>(this.url+'/api/collections/users/records')
      .pipe(
        map((data: any) => {
          return data.items;
        })
      );
  }

  getOneUser(id: string): Observable<any>   {
    return this.http.get<any>(this.url+'/api/collections/users/records/'+id)
      .pipe(
        map((data: any) => {
          return data.items;
        })
      );
  }

}
