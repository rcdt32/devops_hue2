import {Injectable} from '@angular/core';
import {UserServiceService} from "./user-service.service";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  filter = '';
  responseData: any;
  lastMessages: any ;
  constructor(private userService:UserServiceService,
              private http: HttpClient) {
  }

  // eslint-disable-next-line @angular-eslint/contextual-lifecycle

  async getFullMessages() {
    const records = await this.userService.pb.collection('messages').getFullList(200 /* batch size */, {
      sort: '-created',
    });
    console.log(records);
    return records;
  }
  async getMessages() {
    const resultList = await this.userService.pb.collection('messages').getList(1, 50, {
      filter: 'created >= "2022-01-01 00:00:00" && id = "dkd2qw8k64u1ly2"',
    });
    console.log(resultList.items);
    console.log("avec item 0");
    console.log(resultList.items[0].id);
  }

  getAllmessage(): Observable<any>   {
    return this.http.get<any>(this.userService.url+'/api/collections/messages/records')
      .pipe(
        map((data: any) => {
          return data.items;
        })
      );
  }

  getAllmessageByUser(id: string): Observable<any>   {
    return this.http.get<any>(this.userService.url+'/api/collections/messages/records?filter[userId]='+id)
      .pipe(
        map((data: any) => {
          return data.items;
        })
      );
  }

  /**
   * sendet eine nachricht in die datenbank( pocketbase collection messages)
   * @param message
   * @param emphangerId
   * @param senderId
   */
   sendMessage(message: string | null, emphangerId: string, senderId: string): Observable<any> {
     const data = {
       "message": message,
       "senderId": senderId,
       "empfaengerId": emphangerId,
     };
    //const record = await this.userService.pb.collection('messages').create(data);
    //this.lastMessages = ;
    //console.log(record);
    return this.http.post(this.userService.url+'/api/collections/messages/records', data);
  }

  async deleteMessage(id: string) {
    const record = await this.userService.pb.collection('messages').delete(id);
    console.log(record);
  }

  /**
   * holt sich alle nachrichten zwischen zwei user
   * @param senderID
   * @param EmphangerID
   */
  async getChatMessages(senderID: string, EmphangerID: string) {
    this.filter = 'senderId = "'+senderID+'" && empfaengerId = "'+EmphangerID+'"';
    //this.filter = 'senderId = "jefg0ixyht2gogw" && empfaengerId = "jefg0ixyht2gogw"';
    console.log(this.filter);
    const resultList = await this.userService.pb.collection('messages').getList(1, 50, {
      filter: this.filter
    });
    console.log(resultList.items);
    return resultList.items;
  }
}
